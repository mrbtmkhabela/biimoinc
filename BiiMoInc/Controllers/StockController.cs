﻿using BiiMoInc.Models;
using BiiMoInc.Models.View.Stock;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace BiiMoInc.Controllers
{
    [Authorize]
    public class StockController : Controller
    {
        private readonly DatabaseContext _context;

        public StockController(DatabaseContext context)
        {
            _context = context;
        }
        // GET: Stock
        public ActionResult Index()
        {
            using (_context)
            {
                return View(new Index(_context));
            }
        }

        // GET: Stock/Details/5
        public ActionResult Details(Guid SupplierId, Guid ProductId)
        {
            using (_context)
            {
                var model = new Details(_context, SupplierId, ProductId);
                return View(model);
            }
        }

        // GET: Stock/Create
        public ActionResult Create()
        {
            using (_context)
            {
                //TODO:  FIND A CLEANER WAY TO DO THIS
                var model = new Create
                {
                    Products = _context.Products
                    .Where(p => !p.Disabled)
                    .Select(pro => new SelectListItem
                    {
                        Text = pro.Description,
                        Value = pro.Id.ToString()
                    })
                    .OrderBy(pro => pro.Text)
                    .ToList(),

                    Suppliers = _context.Suppliers
                    .Where(s => !s.Disabled)
                    .Select(sup => new SelectListItem
                    {
                        Text = sup.Name,
                        Value = sup.Id.ToString()
                    })
                    .OrderBy(sup => sup.Text)
                    .ToList()
                };

                return View(model);
            }
        }

        // POST: Stock/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Create model)
        {

            using (_context)
            {
                model.Products = _context.Products
                                                 .Where(p => !p.Disabled)
                                                 .Select(pro => new SelectListItem
                                                 {
                                                     Text = pro.Description,
                                                     Value = pro.Id.ToString()
                                                 })
                                                 .OrderBy(pro => pro.Text)
                                                 .ToList();
                model.Suppliers = _context.Suppliers
                                          .Where(s => !s.Disabled)
                                          .Select(sup => new SelectListItem
                                          {
                                              Text = sup.Name,
                                              Value = sup.Id.ToString()
                                          })
                                          .OrderBy(sup => sup.Text)
                                          .ToList();
                try
                {
                    if (!ModelState.IsValid)
                    {
                        return View(model);
                    }
                    // TODO: Add insert logic here
                    {
                        if (_context.Stocks.Find(model.SupplierId, model.ProductId) != null)
                        {
                            ModelState.AddModelError("", "Supplier Product Has Already Been Created");

                            return View(model);
                        }

                        var stock = new Stock
                        {
                            SupplierId = model.SupplierId,
                            ProductId = model.ProductId,
                            UnitPrice = model.UnitPrice,
                            UpdateTimestamp = DateTime.Now,
                            Quantity = model.Quantity
                        };

                        _context.Stocks.Add(stock);
                        _context.SaveChanges();

                        return RedirectToAction(nameof(Index));
                    }
                }
                catch
                {
                    return View();
                }
            }
        }

        // GET: Stock/Edit/5
        public ActionResult Edit(string supplierId, string productId)
        {
            var supId = new Guid(supplierId);
            var prodId = new Guid(productId);

            if (supId == default(Guid) || prodId == default(Guid))
            {
                ModelState.AddModelError("", "Error Occurred Please Reload Page");

                return RedirectToAction(nameof(Index));
            }

            using (_context)
            {
                var dataModel = _context
                                    .Stocks
                                    .Include(prod => prod.Product)
                                    .Include(sup => sup.Supplier)
                                    .First(st => st.SupplierId == supId && st.ProductId == prodId);

                if (dataModel == null)
                {
                    ModelState.AddModelError("", "Error Occurred: Could Not Find Stock");

                    return RedirectToAction(nameof(Index));
                }

                var model = new Edit()
                {
                    UnitPrice = dataModel.UnitPrice,
                    UpdateTimestamp = dataModel.UpdateTimestamp,
                    Quantity = dataModel.Quantity,
                    ProductId = dataModel.ProductId,
                    SupplierId = dataModel.SupplierId,
                    Supplier = dataModel.Supplier.Name,
                Product = dataModel.Product.Description
            };

                return View(model);
            }
        }

        // POST: Stock/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Edit model)
        {
            try
            {
                // TODO: Add update logic here
                if (!ModelState.IsValid)
                {
                    return View(model);
                }

                using (_context)
                {
                    var stock = _context.Stocks.Find(model.SupplierId, model.ProductId);

                    if (stock == null)
                    {
                        ModelState.AddModelError("", "Error Occurred: Could Not Find Stock");

                        return View(model);
                    }

                    stock.Quantity = model.Quantity;
                    stock.UnitPrice = model.UnitPrice;
                    stock.UpdateTimestamp = DateTime.Now;

                    _context.SaveChanges();

                    return RedirectToAction(nameof(Index));
                }
            }
            catch
            {
                return View();
            }
        }
    }
}