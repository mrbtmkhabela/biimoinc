﻿using BiiMoInc.Models;
using BiiMoInc.Models.View.PurchaseOrder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using static BiiMoInc.Constants.Constants;

namespace BiiMoInc.Controllers
{
    public class PurchaseOrderController : Controller
    {

        private readonly DatabaseContext _context;

        public PurchaseOrderController(DatabaseContext context)
        {
            _context = context;
        }

        // GET: PurchaseOrder
        public ActionResult Index()
        {
            using (_context)
            {
                var purchaseOrders = new Index(_context);
                return View(purchaseOrders);
            }
        }

        // GET: PurchaseOrder/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: PurchaseOrder/Create
        public ActionResult Create()
        {
            using (_context)
            {
                var purchaseOrder = new Create(_context);

                return View(purchaseOrder);
            }
        }

        // POST: PurchaseOrder/Create
        [HttpPost]
        public ActionResult CreatePurchaseOrder(string[] orderList)
        {
            try
            {
                using (_context)
                {
                    if (!(orderList.Length > 0))
                    {
                        return RedirectToAction(nameof(Index));
                    }
                    
                    var orderListTemp = new List<Order>();

                    foreach (var item in orderList)
                    {
                        var orderDetailsParts = item.Split('|');

                        if (!Guid.TryParse(orderDetailsParts[0], out Guid purchaseOrderId))
                        {
                            continue;
                        }
                        if (!Guid.TryParse(orderDetailsParts[1], out Guid supplierId))
                        {
                            continue;
                        }
                        if (!Guid.TryParse(orderDetailsParts[2], out Guid productId))
                        {
                            continue;
                        }
                        if (!int.TryParse(orderDetailsParts[3], out int qtyOfProduct))
                        {
                            continue;
                        }

                        var placeOrder = new Order
                        {
                            Id = Guid.NewGuid(),
                            //PurchaseOrderKey = purchaseOrderId.ToString(),
                            PurchaseOrderId = purchaseOrderId,
                            SupplierId = supplierId,
                            ProductId = productId,
                            Quantity = qtyOfProduct,

                        };
                        orderListTemp.Add(placeOrder);
                    }

                    _context.PurchaseOrders.Add(new PurchaseOrder
                    {
                        Id = orderListTemp[0].PurchaseOrderId,
                        OrderDate = DateTime.Now,
                        Status = (int)PurchaseOrderStatus.Pending
                    });
                    _context.SaveChanges();

                    _context.Orders.AddRange(orderListTemp);
                    _context.SaveChanges();
                    return RedirectToAction(nameof(Index));
                }
            }
            catch (Exception ex)
            {

                return RedirectToAction(nameof(Index));
            }

        }
        // GET: PurchaseOrder/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: PurchaseOrder/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: PurchaseOrder/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: PurchaseOrder/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        [HttpGet]
        public ActionResult GetSupplierProducts(Guid supplierId)
        {
            using (_context)
            {
                var stock = _context.Stocks
                                        .Where(st => st.SupplierId == supplierId)
                                        .Select(s =>
                                                new Stocked
                                                {
                                                    ProdId = s.ProductId,
                                                    Name = s.Product.Description,
                                                    Quantity = s.Quantity,
                                                    UnitPrice = s.UnitPrice
                                                })
                                        .ToList();
                return Json(stock);
            }
        }
    }
}