﻿using BiiMoInc.Models;
using BiiMoInc.Models.View.Supplier;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace BiiMoInc.Controllers
{
    [Authorize]
    public class SupplierController : Controller
    {
        private readonly DatabaseContext _context;

        public SupplierController(DatabaseContext context)
        {
            _context = context;
        }

        // GET: Supplier
        public ActionResult Index()
        {
            using (_context)
            {
                return View(new Index(_context));
            }
        }

        // GET: Supplier/Details/5
        public ActionResult Details(Guid id)
        {
            using (_context)
            {
                var model = new Details(_context, id);
                return View(model);
            }
        }

        // GET: Supplier/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Supplier/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Create model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View("Create", model);
                }

                using (_context)
                {
                    var supplier = new Supplier
                    {
                        Id = Guid.NewGuid(),
                        Name = model.Name,
                        Disabled = false
                    };

                    _context.Suppliers.Add(supplier);
                    _context.SaveChangesAsync();

                    return RedirectToAction(nameof(Index));
                }
            }
            catch
            {
                return View();
            }
        }

        // GET: Supplier/Edit/5
        public ActionResult Edit(Guid id)
        {
            using (_context)
            {
                var model = new Edit(_context, id);
                return View(model);
            }
        }

        // POST: Supplier/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Edit model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }
                // TODO: Add update logic here

                using (_context)
                {
                    var supplier = _context.Suppliers.Find(model.Id);

                    if (supplier == null)
                    {
                        ModelState.AddModelError("", "Supplier Not Found");

                        return View(model);
                    }

                    supplier.Name = model.Name;
                    supplier.Disabled = model.Disabled;

                    _context.SaveChanges();

                    return RedirectToAction("Index");
                }
            }
            catch
            {
                return View();
            }
        }

        // GET: Supplier/Delete/5
        
        public ActionResult ChangeStatus(Guid id)
        {
            using (_context)
            {
                var model = new Edit(_context, id);
                if (model == null)
                {
                    ModelState.AddModelError("", "Supplier Not Found");
                    return View(model);
                }
                return View(model);
            }
        }

        // POST: Supplier/Delete/5
       
        
        public ActionResult ChangeStatus(string id)
        {
            try
            {
                // TODO: Add delete logic here
                using (_context)
                {
                    var guid = new Guid(id);
                    var supplier = _context.Suppliers.Find(guid);

                    if (supplier == null)
                    {
                        ModelState.AddModelError("", "Supplier Not Found");

                        return View();
                    }
                    supplier.Disabled = true;

                    _context.SaveChanges();

                    return RedirectToAction(nameof(Index));
                }
            }
            catch
            {
                return View();
            }
        }
    }
}