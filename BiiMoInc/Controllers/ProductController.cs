﻿using BiiMoInc.Models;
using BiiMoInc.Models.View.Product;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace BiiMoInc.Controllers
{
    [Authorize]
    public class ProductController : Controller
    {
        private readonly DatabaseContext _context;

        public ProductController(DatabaseContext context)
        {
            _context = context;
        }

        // GET: Product
        public ActionResult Index()
        {
            using (_context)
            {
                return View(new Index(_context));
            }
        }

        // GET: Product/Details/5
        public ActionResult Details(Guid id)
        {
            using (_context)
            {
                var model = new Details(_context, id);
                return View(model);
            }
        }

        // GET: Product/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Product/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Create model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }

                using (_context)
                {
                    var product = new Product
                    {
                        Id = new Guid(),
                        Description = model.Description,
                        Disabled = model.Disabled,
                        ImagePath = ""
                    };

                    _context.Products.Add(product);
                    _context.SaveChangesAsync();

                    return RedirectToAction(nameof(Index));
                }
            }
            catch
            {
                return View();
            }
        }

        // GET: Product/Edit/5
        public ActionResult Edit(Guid id)
        {
            using (_context)
            {
                var model = new Edit(_context, id);
                return View(model);
            }
        }

        // POST: Product/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Edit model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }
                // TODO: Add update logic here

                using (_context)
                {
                    var product = _context.Products.Find(model.Id);

                    if (product == null)
                    {
                        ModelState.AddModelError("", "Product Not Found");

                        return View(model);
                    }

                    product.Description = model.Description;
                    product.Disabled = model.Disabled;

                    _context.SaveChanges();

                    return RedirectToAction(nameof(Index));
                }
            }
            catch
            {
                return View();
            }
        }
        // GET: Product/Delete/5
        public ActionResult Delete(Guid id)
        {
            using (_context)
            {
                var model = new Edit(_context, id);
                if (model == null)
                {
                    ModelState.AddModelError("", "Product Not Found");
                    return View(model);
                }
                return View(model);
            }
        }

        // POST: Product/Delete/5
        [HttpDelete]
        [ValidateAntiForgeryToken]
        public ActionResult Disable(Guid? id)
        {
            try
            {
                // TODO: Add delete logic here
                using (_context)
                {
                    var product = _context.Products.Find(id);

                    if (product == null)
                    {
                        ModelState.AddModelError("", "Product Not Found");

                        return View();
                    }

                    product.Disabled = true;

                    _context.SaveChanges();

                    return RedirectToAction(nameof(Index));
                }
            }
            catch
            {
                return View();
            }
        }
    }
}