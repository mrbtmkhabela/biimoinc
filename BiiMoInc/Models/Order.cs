﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BiiMoInc.Models
{
    public class Order
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [Column(Order = 2)]
        public Guid ProductId { get; set; }

        [Required]
        [Column(Order = 1)]
        public Guid SupplierId { get; set; }

        [ForeignKey("SupplierId, ProductId")]
        public virtual Stock Stock { get; set; }
        
        [Required]
        public Guid PurchaseOrderId { get; set; }

        [ForeignKey("PurchaseOrderId")]
        public virtual PurchaseOrder PurchaseOrder { get; set; }

        public long Quantity { get; set; }
    }
}
