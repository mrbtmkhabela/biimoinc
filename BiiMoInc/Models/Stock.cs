﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BiiMoInc.Models
{
    public class Stock
    {
        [Key]
        [Column(Order = 2)]
        [Required]
        public Guid ProductId { get; set; }

        [ForeignKey("ProductId")]
        public virtual Product Product { get; set; }

        [Key]
        [Column(Order = 1)]
        [Required]
        public Guid SupplierId { get; set; }

        [ForeignKey("SupplierId")]
        public virtual Supplier Supplier { get; set; }

        public int Quantity { get; set; }

        [RegularExpression(@"^\d+.\d{0,2}$")]
        public float UnitPrice { get; set; }

        public DateTime UpdateTimestamp { get; set; }

    }
}
