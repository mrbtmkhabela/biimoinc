﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Globalization;
using static BiiMoInc.Constants.Constants;

namespace BiiMoInc.Models.ModelBuilders
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Supplier>().HasData(
             new Supplier
             {
                 Id = new Guid("bbb6c86d-070c-45e1-bd59-34d44cc1016e"),
                 Name = "New Age Suppliers (Pty) Ltd"
             },
             new Supplier
             {
                 Id = new Guid("4a9fdd7a-622d-4712-a6ed-1863dacbea54"),
                 Name = "Micky Mouse Inc"
             }
           );

            modelBuilder.Entity<Product>().HasData(
             new Product
             {
                 Id = new Guid("6ea99032-fb2c-44eb-88d0-d400fa6b7e90"),
                 Description = "Bicycle",
                 ImagePath = ""
             },
             new Product
             {
                 Id = new Guid("8b89b77d-ac63-40aa-9607-e0cc852506ec"),
                 Description = "Smart Watch",
                 ImagePath = ""
             }
           );

            modelBuilder.Entity<Stock>()
                .HasKey(s => new { s.SupplierId, s.ProductId });

            modelBuilder.Entity<Stock>()
                .HasData(
             new Stock
             {
                 ProductId = new Guid("6ea99032-fb2c-44eb-88d0-d400fa6b7e90"),
                 SupplierId = new Guid("bbb6c86d-070c-45e1-bd59-34d44cc1016e"),
                 Quantity = 100,
                 UnitPrice = (float)890.00,
                 UpdateTimestamp = DateTime.Now
             },
             new Stock
             {
                 ProductId = new Guid("6ea99032-fb2c-44eb-88d0-d400fa6b7e90"),
                 SupplierId = new Guid("4a9fdd7a-622d-4712-a6ed-1863dacbea54"),
                 Quantity = 50,
                 UnitPrice = (float)800.00,
                 UpdateTimestamp = DateTime.Now

             },
             new Stock
             {
                 ProductId = new Guid("8b89b77d-ac63-40aa-9607-e0cc852506ec"),
                 SupplierId = new Guid("4a9fdd7a-622d-4712-a6ed-1863dacbea54"),
                 Quantity = 10,
                 UnitPrice = (float)2500.00,
                 UpdateTimestamp = DateTime.Now
             }
           );
            modelBuilder.Entity<PurchaseOrder>().HasData(

                new PurchaseOrder
                {
                    Id = new Guid("e7b00545-ad99-4eac-baf6-c6db55af2bbf"),
                    OrderDate = DateTime.ParseExact("18/09/2019 10:10:24",
                                "dd/MM/yyyy HH:mm:ss",
                                CultureInfo.InvariantCulture),
                    DeliveryDate = DateTime.ParseExact("20/09/2019 13:35:24",
                                "dd/MM/yyyy HH:mm:ss",
                                CultureInfo.InvariantCulture),
                    Status = (int)PurchaseOrderStatus.Pending,
                },
                new PurchaseOrder
                {
                    Id = new Guid("b115e5aa-92bb-4373-be73-d4b687edb19f"),
                    OrderDate = DateTime.ParseExact("10/08/2019 12:02:11",
                                "dd/MM/yyyy HH:mm:ss",
                                CultureInfo.InvariantCulture),
                    DeliveryDate = DateTime.ParseExact("14/08/2019 08:00:36",
                                "dd/MM/yyyy HH:mm:ss",
                                CultureInfo.InvariantCulture),
                    Status = (int)PurchaseOrderStatus.Closed,
                }
            );

            modelBuilder.Entity<Order>()
                .HasOne(p => p.PurchaseOrder)
                .WithMany(b => (System.Collections.Generic.IEnumerable<Order>)b.Order);

            modelBuilder.Entity<Order>().HasData(

                new Order
                {
                    Id = new Guid("10dff704-7286-478f-8a0c-83e8e1af323e"),
                    PurchaseOrderId = new Guid("e7b00545-ad99-4eac-baf6-c6db55af2bbf"),
                    SupplierId = new Guid("4a9fdd7a-622d-4712-a6ed-1863dacbea54"),
                    ProductId = new Guid("8b89b77d-ac63-40aa-9607-e0cc852506ec"),
                    Quantity = 5,
                },
                new Order
                {
                    Id = new Guid("82d1af1d-3de6-48a0-95b0-790bbeb29c0e"),
                    PurchaseOrderId = new Guid("b115e5aa-92bb-4373-be73-d4b687edb19f"),
                    SupplierId = new Guid("bbb6c86d-070c-45e1-bd59-34d44cc1016e"),
                    ProductId = new Guid("6ea99032-fb2c-44eb-88d0-d400fa6b7e90"),
                    Quantity = 15,
                }

            );
        }
    }
}
