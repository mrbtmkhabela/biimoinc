﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BiiMoInc.Models
{
    public class PurchaseOrder
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public DateTime OrderDate { get; set; }

        public virtual Order Order { get; set; }

        [Required]
        public DateTime DeliveryDate { get; set; }

        [Required]
        public int Status { get; set; }
    }
}
