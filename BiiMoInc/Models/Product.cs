﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BiiMoInc.Models
{
    public class Product
    {
        [Key]
        public Guid Id { get; set; }

        public string Description { get; set; }
        
        public string ImagePath { get; set; }

        [Required]
        [DefaultValue(false)]
        public bool Disabled { get; set; }
    }
}


