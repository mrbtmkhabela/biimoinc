﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using static BiiMoInc.Constants.Constants;

namespace BiiMoInc.Models.View.PurchaseOrder
{
    public class Create
    {
        public Guid Id { get; set; }
        public Guid SupplierId { get; set; }
        public DateTime DateCreated { get; set; }
        public PurchaseOrderStatus Status { get; set; }
        public List<SelectListItem> Suppliers { get; set; }
        public List<Stocked> StockedItems { get; set; }

        public Create(DatabaseContext _context)
        {
            Id = Guid.NewGuid();
            DateCreated = DateTime.Now;
            Status = PurchaseOrderStatus.Pending;
            Suppliers = _context.Suppliers
                                 .Where(sup => !sup.Disabled)
                                 .Select(s =>
                                     new SelectListItem
                                     {
                                         Text = s.Name,
                                         Value = s.Id.ToString()
                                     })
                                 .OrderBy(sup => sup.Text)
                                 .ToList();

            var firstSupplier = new Guid(Suppliers.FirstOrDefault()?.Value);

            StockedItems = _context.Stocks.Where(s => s.SupplierId == firstSupplier && !s.Product.Disabled)
                                          .Select(st => new Stocked
                                          {
                                              ProdId = st.ProductId,
                                              Name = st.Product.Description,
                                              Quantity = st.Quantity,
                                              UnitPrice = st.UnitPrice
                                          }).ToList();
        }

        public Create()
        {
        }
    }

    public class Stocked
    {
        public Guid ProdId { get; set; }
        public string Name { get; set; }
        public float UnitPrice { get; set; }
        public int Quantity { get; set; }
        public int RequestedQuantiy { get; set; }
    }
}
