﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using static BiiMoInc.Constants.Constants;

namespace BiiMoInc.Models.View.PurchaseOrder
{
    public class Index
    {
        public List<UserPurchaseOrder> PurchaseOrders { get; set; }

        public Index(DatabaseContext context)
        {
            try
            {
                PurchaseOrders  = context.Orders
                    .Include(p => p.Stock)
                    .Include(p => p.Stock.Supplier)
                            .GroupBy(po => new { po.PurchaseOrder, po.Stock.Supplier, po.Stock })
                            .Select(o =>
                                new UserPurchaseOrder
                                {
                                    Id = o.Key.PurchaseOrder.Id,
                                    DateCreated = o.Key.PurchaseOrder.OrderDate,
                                    Status = ((PurchaseOrderStatus)o.Key.PurchaseOrder.Status).ToString(),
                                    TotalQuatity = o.Sum(or => or.Quantity),
                                    Supplier = o.Key.Supplier.Name,
                                    TotalValue = o.Sum( or => or.Quantity * o.Key.Stock.UnitPrice)
                                }
                                )
                            .OrderByDescending(p => p.DateCreated)
                            .ToList();

            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }

    public class UserPurchaseOrder
    {
        public Guid Id { get; set; }
        public DateTime DateCreated { get; set; }
        public string Status { get; set; }
        public string Supplier { get; set; }
        public long TotalQuatity { get; set; }
        public float TotalValue { get; set; }
    }
}
