﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;

namespace BiiMoInc.Models.View.PurchaseOrder
{
    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class UserOrder
    {
        public string PurchaseOrderId { get; set; }
        public string SupplierId { get; set; }
        public string ProductId { get; set; }
        public int QtyOfProduct { get; set; }
        public DateTime OrderTimestamp { get; set; }
    }
}
