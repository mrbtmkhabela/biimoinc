﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BiiMoInc.Models.View.Stock
{
    public abstract class BaseStock
    {
        [Required]
        [Display(Name = "Product")]
        public Guid ProductId { get; set; }

        [Required]
        [Display(Name = "Supplier")]
        public Guid SupplierId { get; set; }

        [Display(Name = "Total Number In Stock")]
        public int Quantity { get; set; }

        [Display(Name = "Price Per Unit")]
        public float UnitPrice { get; set; }

        [Display(Name = "Last Updated Date")]
        public DateTime UpdateTimestamp { get; set; }
    }
}
