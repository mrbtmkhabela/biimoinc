﻿using System;
using System.Linq;

namespace BiiMoInc.Models.View.Stock
{
    public class Details : BaseStock
    {

        public Details(DatabaseContext context, Guid SupplierId, Guid ProductId)
        {

            var stock = context.Stocks
               .FirstOrDefault(st => st.ProductId == ProductId && st.SupplierId == SupplierId);

            Quantity = (int)stock.Quantity;
            UpdateTimestamp = stock.UpdateTimestamp;
            UnitPrice = stock.UnitPrice;
        }
    }
}
