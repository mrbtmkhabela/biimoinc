﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Linq;

namespace BiiMoInc.Models.View.Stock
{
    public class Create : BaseStock
    {
        public List<SelectListItem> Suppliers { get; set; }

        public List<SelectListItem> Products { get; set; }

        public Create()
        {

        }

        public Create(DatabaseContext context)
        {
            Products = context.Products
               .Where(p => !p.Disabled)
               .Select(pro => new SelectListItem
               {
                   Text = pro.Description,
                   Value = pro.Id.ToString()
               })
               .OrderBy(pro => pro.Text)
               .ToList();

            Suppliers = context.Suppliers
                .Where(s => !s.Disabled)
                .Select(sup => new SelectListItem
                {
                    Text = sup.Name,
                    Value = sup.Id.ToString()
                })
                .OrderBy(sup => sup.Text)
                .ToList();
        }
    }
}