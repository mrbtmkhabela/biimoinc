﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace BiiMoInc.Models.View.Stock
{
    public class Index : BaseStock
    {
        public string Product { get; set; }

        public string Supplier { get; set; }
        public List<Index> Stock { get; set; }

        public Index(DatabaseContext context)
        {
            Stock = context
                     .Stocks
                     .Where(st => !st.Product.Disabled && !st.Supplier.Disabled)
                     .Include(p => p.Product)
                     .Include(s => s.Supplier)
                     .Select(stk =>
                              new Index
                              {
                                  SupplierId = stk.SupplierId,
                                  Supplier = stk.Supplier.Name,
                                  ProductId = stk.ProductId,
                                  Product = stk.Product.Description,
                                  Quantity = (int)stk.Quantity,
                                  UnitPrice = stk.UnitPrice,
                                  UpdateTimestamp = stk.UpdateTimestamp
                              })
                     .OrderBy(s => s.UpdateTimestamp)
                     .ToList();
        }

        public Index()
        {
        }
    }
}
