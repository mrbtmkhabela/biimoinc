﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BiiMoInc.Models.View.Stock
{
    public class Edit : BaseStock
    {
        public string Product { get; set; }

        public string Supplier { get; set; }

        public Edit(DatabaseContext context, Guid SupplierId, Guid ProductId)
        {
            var stock = context.Stocks
                                .Include(prod => prod.Product)
                                .Include(sup => sup.Supplier)
                                .FirstOrDefault(st => st.SupplierId == SupplierId && st.ProductId == ProductId);

            Supplier = stock.Supplier.Name;
            Product = stock.Product.Description;
            Quantity = (int)stock.Quantity;
            UpdateTimestamp = stock.UpdateTimestamp;
            UnitPrice = stock.UnitPrice;
        }
        public Edit()
        {

        }
    }
}
