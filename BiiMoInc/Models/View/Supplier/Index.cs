﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BiiMoInc.Models.View.Supplier
{
    public class Index : BaseSupplier
    {
        public Guid Id { get; set; }
        public List<Index> Suppliers { get; set; }

        public Index(DatabaseContext context)
        {
            Suppliers = context
                            .Suppliers
                            .Select(sup =>
                                     new Index
                                     {
                                         Id = sup.Id,
                                         Name = sup.Name,
                                         Disabled = sup.Disabled

                                     })
                            .ToList();
        }

        public Index()
        {

        }
    }
}
