﻿using System.ComponentModel.DataAnnotations;

namespace BiiMoInc.Models.View.Supplier
{
    public abstract class BaseSupplier
    {
        [Display(Name = "Supplier")]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Active")]
        public bool Disabled { get; set; }
    }
}
