﻿using System;
using System.Linq;

namespace BiiMoInc.Models.View.Supplier
{
    public class Edit : BaseSupplier
    {
        public Guid Id { get; set; }

        public Edit(DatabaseContext context, Guid id)
        {
            var supplier = context.Suppliers.Where(s => s.Id == id).FirstOrDefault();

            Id = supplier.Id;
            Name = supplier.Name;
            Disabled = supplier.Disabled;
        }
        public Edit ()
        {

        }
    }
}
