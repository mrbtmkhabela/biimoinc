﻿using System;
using System.Linq;

namespace BiiMoInc.Models.View.Supplier
{
    public class Details : BaseSupplier
    {
        public Guid Id { get; set; }

        public Details(DatabaseContext context, Guid id)
        {
            var supplier = context.Suppliers.Where(s => s.Id == id).First();

            Id = supplier.Id;
            Name = supplier.Name;
            Disabled = supplier.Disabled;
        }

    }
}
