﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BiiMoInc.Models.View.Supplier
{
    public class Create 
    {
        public Guid Id { get; set; }
        [Display(Name = "Supplier")]
        [Required]
        public string Name { get; set; }

    }
}
