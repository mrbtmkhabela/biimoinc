﻿using System;
using System.Linq;

namespace BiiMoInc.Models.View.Product
{
    public class Edit : BaseProduct
    {
        public Guid Id { get; set; }

        public Edit(DatabaseContext context, Guid id)
        {
            var product = context.Products.Where(s => s.Id == id).First();

            Id = product.Id;
            Description = product.Description;
            ImagePath = product.ImagePath;
            Disabled = product.Disabled;
        }
        public Edit()
        {

        }
    }
}
