﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BiiMoInc.Models.View.Product
{
    public class Index : BaseProduct
    {
        public Guid Id { get; set; }
        public List<Index> Products { get; set; }

        public Index(DatabaseContext context)
        {
            Products = context
                            .Products
                            .Select(prod =>
                                     new Index
                                     {
                                         Id = prod.Id,
                                         Description = prod.Description,
                                         Disabled = prod.Disabled
                                     })
                            .ToList();
        }

        public Index()
        {
        }
    }
}
