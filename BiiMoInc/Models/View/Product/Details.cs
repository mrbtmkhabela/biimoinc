﻿using System;
using System.Linq;

namespace BiiMoInc.Models.View.Product
{
    public class Details : BaseProduct
    {
        public Guid Id { get; set; }

        public Details(DatabaseContext context, Guid id)
        {
            var product = context.Products.Where(s => s.Id == id).First();

            Id = product.Id;
            Description = product.Description;
            Disabled = product.Disabled;
        }
    }
}
