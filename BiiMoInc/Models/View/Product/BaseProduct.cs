﻿using System.ComponentModel.DataAnnotations;

namespace BiiMoInc.Models.View.Product
{
    public abstract class BaseProduct
    {
        [Display(Name = "Product")]
        public string Description { get; set; }

        [Display(Name = "Image")]
        public string ImagePath { get; set; }

        [Display(Name = "Active")]
        public bool Disabled { get; set; }
    }
}
