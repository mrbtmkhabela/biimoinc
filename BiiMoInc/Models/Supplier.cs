﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BiiMoInc.Models
{
    public class Supplier
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        [DefaultValue(false)]
        public bool Disabled { get; set; }
    }
}
