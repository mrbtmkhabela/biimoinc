var biimo;
(function (biimo) {
    "use strict";
    var DropdownListFilter = /** @class */ (function () {
        function DropdownListFilter() {
        }
        DropdownListFilter.setDropdown = function (dropdown, data, showInstruction) {
            if (showInstruction === void 0) { showInstruction = false; }
            var i = 0, l = data.length, optElem, item;
            dropdown.innerHTML = "";
            if (l === 0) {
                optElem = document.createElement("option");
                optElem.text = "None";
                optElem.disabled = true;
                dropdown.add(optElem);
                return;
            }
            if (showInstruction && l > 0) {
                optElem = document.createElement("option");
                optElem.value = "";
                optElem.text = "Please Select";
                dropdown.add(optElem);
            }
            for (i = 0; i < l; i += 1) {
                item = data[i];
                optElem = document.createElement("option");
                optElem.value = item.Value;
                optElem.text = item.Text;
                dropdown.add(optElem);
            }
        };
        return DropdownListFilter;
    }());
    biimo.DropdownListFilter = DropdownListFilter;
})(biimo || (biimo = {}));
//# sourceMappingURL=Biimo.DropDownhelper.js.map