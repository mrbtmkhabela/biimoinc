﻿module biimo {
    "use strict";
    interface IOption {
        Value: string;
        Text: string;
    }

    export class DropdownListFilter {
        static setDropdown(dropdown: HTMLSelectElement, data: IOption[], showInstruction: boolean = false) {
            var i: number = 0,
                l: number = data.length,
                optElem: HTMLOptionElement,
                item: IOption;

            dropdown.innerHTML = "";

            if (l === 0) {
                optElem = document.createElement("option");
                optElem.text = "None";
                optElem.disabled = true;
                dropdown.add(optElem);
                return;
            }
            if (showInstruction && l > 0) {
                optElem = document.createElement("option");
                optElem.value = "";
                optElem.text = "Please Select";
                dropdown.add(optElem);
            }
            for (i = 0; i < l; i += 1) {
                item = data[i];
                optElem = document.createElement("option");
                optElem.value = item.Value;
                optElem.text = item.Text;
                dropdown.add(optElem);
            }

        }

    }

}