# Setup
- Project is a Code First Db application
- Run Migrations when starting up
- Create User to login


# Settings
|Key   	|Value   	|
|---	|---	|
|DefaultConnection   	|"Change to Local Db (Project is a code first, but theres a copy of the db within the project path(~\BiiMoInc\Data\DbScript))"	|

# Databas Tables

Suppliers Table


| Id | Name | 
| ------ | ------ |
| ID | Name of Supplier |

Products Table 


| Id | Description|
| ------ | ------ |
| idcell | Description of the Product |

 Purchase Orders

| Id | OrderDate| DeliveryDate |Status|
| ------ | ------ | ------ | ------ |
| ID | Order Date |DeliveryDate | Status Order |

Stock

| Id | ProductId(Fk)| SupplierId (FK)| Quantity |UnitPrice | UpdateTimestamp|
| ------ | ------ | ------ | ------ |------ |------ |
| ID | ID of Associated Product |ID of Associated Supplier | Qty | Unit Price of Product |Date on which any change to the Order is done |
